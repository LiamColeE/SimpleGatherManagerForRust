namespace Oxide.Plugins
{
    [Info("SimpleGatherManager", "LiamColeE", 0.1)]
    [Description("Manages Gather Rates")]

    class SimpleGatherManager : RustPlugin
    {
        //THESE VARIABLES ARE YOUR GATHER RATES
        //
        const int GATHER_RATES = 5;
        const int NIGHT_GATHER_RATES = 7;
        const int QUARRY_GATHER_RATES = 10;
        //
        //THESE VARIABLES ARE YOUR GATHER RATES

        //DELETE IN BETWEEN THIS COMMENT TO REMOVE INSTA CRAFT
        //
        void OnItemCraft(ItemCraftTask item)
        {
            item.endTime = 1f; 
        }
        //
        //DELETE IN BETWEEN THIS COMMENT TO REMOVE INSTA CRAFT
        void OnDispenserGather(ResourceDispenser dispenser, BaseEntity entity, Item item)
        {
            if (TOD_Sky.Instance.IsNight)
            {
                item.amount = (int)(item.amount * NIGHT_GATHER_RATES);
            }
            else
            {
                item.amount = (int)(item.amount * GATHER_RATES);
            }           
        }

        void OnCropGather(PlantEntity plant, Item item, BasePlayer player)
        {
            item.amount = (int)(item.amount * GATHER_RATES);
        }

        void OnCollectiblePickup(Item item, BasePlayer player)
        {
            item.amount = (int)(item.amount * GATHER_RATES);           
        }   

        void OnQuarryGather(MiningQuarry quarry, Item item)
        {
            item.amount = (int)(item.amount * QUARRY_GATHER_RATES);
        }
    }
}